# sudoloop

`sudoloop` is intended for long-running scripts which run as one user but occasionally use `sudo` to perform actions as another user. This works by calling `sudo --validate` in a loop to keep the `sudo` session alive while the script is running. For maximum portability, `sudoloop` is written to work on any POSIX shell.

The simplest usage simply involves wrapping the command in `sudoloop`:

```shell
sudoloop command [arg]...
```

The `SUDO_LOOP_TIME` environment variable can be used to change the amount of time spent between `sudo --validate` calls, in case the default time of 60 seconds is too short. The `SUDO_LOOP_TIME` variable can take any string that is a valid argument to the `sleep` command, which defaults to seconds:

```shell
env SUDO_LOOP_TIME=30 sudoloop command [arg]...
```

## License

Available under the public domain via the [CC0 Universal License].

[CC0 Universal License]: ./LICENSE.md
